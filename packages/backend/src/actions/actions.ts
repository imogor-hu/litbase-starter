import { registry, RpcRequest } from "@litbase/core";
import { customAction } from "@my-app/common/actions/actions";

registry.register(customAction, async (request: RpcRequest) => {
  console.log("Action running on the backend");
});
