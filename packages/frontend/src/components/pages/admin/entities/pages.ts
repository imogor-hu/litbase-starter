import { BookContent } from "@styled-icons/boxicons-regular";
import { Entity, ValueType } from "../common/types";
import { Collections } from "@my-app/common/models/collections";

export const pagesEntity: Entity = {
  name: "pages",
  humanReadableName: "Pages",
  icon: BookContent,
  fields: {
    name: {
      displayName: "Név",
      type: ValueType.string,
      isIncludedInListPage: true,
    },
    url: {
      displayName: "Elérési útvonal",
      type: ValueType.string,
      isIncludedInListPage: true,
    },
    backgroundImage: {
      displayName: "Háttérkép",
      type: ValueType.file,
      isIncludedInListPage: false,
    },
    blocks: {
      displayName: "Blokkok",
      type: ValueType.blocks,
      isIncludedInListPage: true,
      config: {
        hero: {
          fields: {
            backgroundImage: {
              type: ValueType.file,
              multi: false,
              isIncludedInListPage: true,
              displayName: "Háttérkép",
            },
          },
          displayName: "Kedő oldal blokk",
          explainerImage: "",
        },
      },
    },
  },
  nameField: "name",
  collectionName: Collections.PAGES,
  searchableFields: ["name"],
} as Entity;
