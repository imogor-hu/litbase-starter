import { News, User } from "@styled-icons/boxicons-regular";
import { Entity, ValueType } from "../common/types";
import { Collections } from "@my-app/common/models/collections";

export const userEntity: Entity = {
  name: "users",
  humanReadableName: "Users",
  icon: User,
  fields: {
    name: {
      displayName: "Név",
      type: ValueType.string,
      isIncludedInListPage: true,
    },
    isAdmin: {
      displayName: "Admin",
      type: ValueType.boolean,
      isIncludedInListPage: true,
    },
  },
  nameField: "name",
  collectionName: Collections.USERS,
  searchableFields: ["name"],
};
