import { createContext, useContext } from "react";
import { AdminConfig } from "./types";

export const AdminContext = createContext<AdminConfig>({} as unknown as AdminConfig);

export function useAdminConfig() {
  return useContext(AdminContext);
}
