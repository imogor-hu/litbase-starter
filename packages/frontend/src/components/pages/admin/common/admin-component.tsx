import { Fragment } from "react";
import { Switch, Route } from "react-router-dom";
import { AdminConfig } from "./types";
import { AdminEditPage } from "../edit/admin-edit-page";
import { AdminSidebar } from "../sidebar/admin-sidebar";
import { AdminContext } from "./admin-context";
import styled from "styled-components";
import { AdminListPage } from "../list/admin-list-page";

export function AdminComponent(config: AdminConfig) {
  return (
    <AdminContext.Provider value={config}>
      <Body>
        <AdminSidebar />
        <Switch>
          {config.entities.map((entity, index) => (
            <Route
              key={index}
              path={config.prefix + "/admin/" + entity.name}
              exact
              render={() => <AdminListPage entity={entity} />}
            />
          ))}
          {config.entities.map((entity, index) => (
            <Route
              key={-index}
              path={config.prefix + "/admin/" + entity.name + "/:id"}
              exact
              render={({
                match: {
                  params: { id },
                },
              }) => <AdminEditPage entity={entity} id={id || ""} />}
            />
          ))}
        </Switch>
      </Body>
    </AdminContext.Provider>
  );
}

const Body = styled.div`
  display: flex;
`;
