import { createBinaryId } from "@litbase/core";
import { StyledIcon } from "@styled-icons/styled-icon";
import { Block, BlockType } from "@my-app/common/models/page";
import { ComponentType } from "react";

export enum ValueType {
  string = "string",
  number = "number",
  date = "date",
  richText = "richText",
  file = "file",
  boolean = "boolean",
  blocks = "blocks",
}

type FieldTypeBase = {
  type: ValueType;
  displayName: string;
  isIncludedInListPage?: boolean;
};

export type FileFieldType = FieldTypeBase & {
  type: ValueType.file;
  multi: boolean;
};

export type BlockFieldType = FieldTypeBase & {
  type: ValueType.blocks;
  config: Record<string, { fields: Fields; displayName: string; explainerImage: string }>;
};

export type FieldType = FieldTypeBase | FileFieldType | BlockFieldType;

export type Fields = {
  type: never;
  [name: string]: FieldType;
};

export interface BlockConfigValues<T extends Block = Block> {
  createEmpty: (id: number) => T;
  displayName: string;
  explainerImage: string;
  editor: ComponentType<{ name: string }>;
}

export interface BlockConfig {
  [type: BlockType]: BlockConfigValues;
}

export interface Entity {
  fields: Fields;
  name: string;
  collectionName: string;
  humanReadableName: string;
  searchableFields: string[];
  nameField: string;
  slugField?: string;
  icon?: StyledIcon;
}

export type PrimitiveValue = string | number | Date;

export function createEmptyEntity(entity: Entity) {
  return {
    _id: createBinaryId(),
    ...(createEmptyValue(entity.fields) as Record<string, unknown>),
  };
}

export function createEmptyValue(value: Fields | FieldType): Record<string, unknown> | PrimitiveValue {
  if (!value.type) {
    return Object.fromEntries(Object.entries(value as Fields).map(([key, value]) => [key, createEmptyValue(value)]));
  }
  switch ((value as { type: ValueType }).type) {
    case ValueType.number:
      return 0;
    case ValueType.date:
      return new Date();
    case ValueType.blocks:
      return [];
    default:
    case ValueType.string:
      return "";
  }
}

export interface AdminConfig {
  entities: Entity[];
  prefix?: string;
  logoUrl?: string;
  name: string;
}
