import { ReactNode, useRef } from "react";
import { transitions } from "polished";
import { Instance } from "tippy.js";
import Tippy from "@tippyjs/react";
import { ChevronDown } from "@styled-icons/boxicons-regular";
import { Button, ButtonProps, FontWeight, spacing2, spacing4 } from "@litbase/alexandria";
import styled from "styled-components";

export interface SelectButtonProps<T = unknown> extends ButtonProps {
  onSelect?(value: T): void;
  options?: { label: ReactNode; value: T }[];
  header?: ReactNode;
}

export function SelectButton<T>({ children, onSelect, options, header, ...props }: SelectButtonProps<T>) {
  const tippyRef = useRef<Instance | null>(null);
  return (
    <AddBlockTippy
      trigger="click"
      onCreate={(tippy) => (tippyRef.current = tippy)}
      interactive
      content={
        <SelectButtonPopup<T>
          options={options}
          header={header}
          onSelect={(value) => {
            tippyRef.current?.hide();
            onSelect?.(value);
          }}
        />
      }
    >
      <StyledButton {...props}>
        {children}
        <ButtonChevron />
      </StyledButton>
    </AddBlockTippy>
  );
}

const StyledButton = styled(Button)`
  white-space: nowrap;
`;

// hack: theme has to be given here, otherwise the theme prop conflicts with the styled-components theme prop
// in typing and TS says there's an error, when there really is none
const AddBlockTippy = styled(Tippy).attrs(() => ({ theme: "light-border" }))`
  overflow: hidden;

  .tippy-content {
    padding: 0;
  }
`;

const ButtonChevron = styled(ChevronDown)`
  margin-left: ${spacing2};
  margin-right: 0 !important;
`;

export function SelectButtonPopup<T = unknown>({
  options,
  header,
  onSelect,
}: {
  options?: { label: ReactNode; value: T }[];
  header?: ReactNode;
  onSelect?(value: T): void;
}) {
  return (
    <StyledSelectButtonPopup>
      {header && <SelectButtonPopupHeader>{header}</SelectButtonPopupHeader>}
      {options?.map((entry, index) => (
        <SelectButtonPopupOption key={index} onClick={() => onSelect?.(entry.value)}>
          {entry.label}
        </SelectButtonPopupOption>
      ))}
    </StyledSelectButtonPopup>
  );
}

const SelectButtonPopupHeader = styled.div`
  padding: ${spacing2} ${spacing4};
  border-bottom: 1px solid ${({ theme }) => theme.gray200};
  text-transform: uppercase;
  font-size: ${({ theme }) => theme.textXs};
  color: ${({ theme }) => theme.gray500};
  font-weight: ${FontWeight.Medium};
  background-color: ${({ theme }) => theme.gray50};
`;

const StyledSelectButtonPopup = styled.div`
  font-weight: normal;
  text-transform: none;
  white-space: nowrap;
`;

const SelectButtonPopupOption = styled.div`
  padding: ${spacing2} ${spacing4};
  font-size: ${({ theme }) => theme.textSm};
  cursor: pointer;
  ${({ theme }) => transitions(["background-color"], theme.defaultTransitionTiming)};

  &:hover {
    background-color: ${({ theme }) => theme.gray100};
  }
`;
