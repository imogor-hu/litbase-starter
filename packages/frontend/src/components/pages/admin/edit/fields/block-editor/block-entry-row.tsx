import { memo } from "react";
import {
  Cell,
  CollapsibleGridRow,
  CollapsibleGridRowHeader,
  CollapsibleIndicatorCell,
  DraggableListRow,
  FontWeight,
  ReorderableListHandle,
  useFastField,
} from "@litbase/alexandria";
import { createSlug } from "../../../../../../utils/create-slug";
import styled from "styled-components";
import { BlockType } from "@my-app/common/models/page";
import { BlockEntryBody } from "./block-entry-body";
import { useBlockConfig } from "./block-context";

export const BlockEntryRow = memo(BlockEntryRowComponent);

function BlockEntryRowComponent({
  index,
  id,
  onRemove,
}: {
  index: number;
  id: string | number;
  onRemove(index: number): void;
}) {
  return (
    <DraggableListRow draggableId={id.toString()} index={index}>
      {(provided) => (
        <CollapsibleGridRow>
          <CollapsibleGridRowHeader {...provided.draggableProps} ref={provided.innerRef}>
            <Cell {...provided.dragHandleProps}>
              <ReorderableListHandle />
            </Cell>
            <Cell>
              <BlockTitle name={`blocks.${index}.title`} />
            </Cell>
            <Cell>
              <BlockTypeLabel name={`blocks.${index}.type`} />
            </Cell>
            <Cell>
              <BlockLink index={index} />
            </Cell>
            <CollapsibleIndicatorCell />
          </CollapsibleGridRowHeader>
          <BlockEntryBody index={index} onRemove={onRemove} />
        </CollapsibleGridRow>
      )}
    </DraggableListRow>
  );
}

function BlockLink({ index }: { index: number }) {
  const [{ value: blockName }] = useFastField(`blocks.${index}.title`);
  const [{ value: pageUrl }] = useFastField("url");
  const blockSlug = createSlug(blockName);

  if (!blockSlug) return <>N/A</>;

  const path = (pageUrl + "/" + blockSlug).replace(/(\/)+/g, "");
  const url = location.protocol + "://" + location.host + path;
  return (
    <a href={url} target="_blank">
      /{path}
    </a>
  );
}

function BlockTitle({ name }: { name: string }) {
  const [{ value: title }] = useFastField(name);

  return <StyledBlockTitle $isUnnamed={!title}>{title || "Névtelen"}</StyledBlockTitle>;
}

const StyledBlockTitle = styled.div<{ $isUnnamed: boolean }>`
  font-weight: ${({ $isUnnamed }) => ($isUnnamed ? FontWeight.Normal : FontWeight.Medium)};
  color: ${({ theme, $isUnnamed }) => ($isUnnamed ? theme.gray400 : "inherit")};
`;

function BlockTypeLabel({ name }: { name: string }) {
  const { blockConfig } = useBlockConfig();
  const [{ value: type }] = useFastField<BlockType>(name);
  if (!type) {
    return null;
  }
  return <>{blockConfig[type].displayName}</>;
}
