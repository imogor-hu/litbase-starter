import { createContext, useContext } from "react";
import { BlockConfig } from "../../../common/types";

export const BlockContext = createContext<{ blockConfig: BlockConfig }>({ blockConfig: {} });

export function useBlockConfig() {
  return useContext(BlockContext);
}
