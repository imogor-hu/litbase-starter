import { Trash } from "@styled-icons/boxicons-regular";
import { BlockType } from "@my-app/common/models/page";
import {
  Button,
  CollapsibleContentButtonContainer,
  ConfirmDeleteButton,
  useFastField,
  CollapsibleGridRowContent,
} from "@litbase/alexandria";
import { getTranslatableDisplayText } from "@litbase/alexandria/services/translation-service";
import { useBlockConfig } from "./block-context";

export function BlockEntryBody({ index, onRemove }: { index: number; onRemove(index: number): void }) {
  const name = "blocks." + index;

  return (
    <CollapsibleGridRowContent>
      <BlockContentEditor name={name} />
      <CollapsibleContentButtonContainer>
        <DeleteBlockButton name={name} index={index} onRemove={onRemove} />
      </CollapsibleContentButtonContainer>
    </CollapsibleGridRowContent>
  );
}

export function BlockContentEditor({ name }: { name: string }) {
  const [{ value: type }] = useFastField<BlockType>(name + ".type");
  const { blockConfig } = useBlockConfig();
  const EditorComponent = blockConfig[type]?.editor;

  return EditorComponent ? <EditorComponent name={name} /> : null;
}

function DeleteBlockButton({ name, index, onRemove }: { name: string; index: number; onRemove(index: number): void }) {
  const [{ value: title }] = useFastField(name + ".title");
  const titleText = getTranslatableDisplayText(title);

  return (
    <ConfirmDeleteButton
      onConfirm={() => onRemove(index)}
      title="Blokk törlése"
      content={
        <span>
          {titleText ? (
            <>Biztosan törölni akarod a(z) {titleText} blokkot?</>
          ) : (
            <>Biztosan törölni akarod ezt a blokkot?</>
          )}
        </span>
      }
      component={DeleteBlockButtonComponent}
    />
  );
}

function DeleteBlockButtonComponent({ onClick }: { onClick(): void }) {
  return (
    <Button $kind="danger" onClick={onClick}>
      <Trash /> Blokk törlése
    </Button>
  );
}
