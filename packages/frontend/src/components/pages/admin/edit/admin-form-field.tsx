import { BlockFieldType, FieldType, FileFieldType, ValueType } from "../common/types";
import {
  DateField,
  FieldWrapper,
  FileEntry,
  FileUploadField,
  InputField,
  RichTextEditorField,
  SingleFileUploadField,
  SwitchField,
  TransitionStatus,
} from "@litbase/alexandria";
import { map } from "rxjs/operators";
import { BlockEditorField } from "./fields/block-editor/block-editor-field";
import { client } from "@litbase/client";

export function AdminFormField({ value, name }: { value: FieldType; name: string }) {
  const { component, props } = getComponent(value);
  // @ts-ignore
  return <FieldWrapper name={name} component={component} label={value.displayName} {...props} />;
}

function getComponent(value: FieldType) {
  switch (value.type) {
    case ValueType.number:
      return { component: InputField, props: { type: "number" } };
    case ValueType.date:
      return { component: DateField };
    case ValueType.string:
      return { component: InputField };
    case ValueType.richText:
      return { component: RichTextEditorField };
    case ValueType.boolean:
      return { component: SwitchField };
    case ValueType.blocks:
      return { component: BlockEditorField, props: { blockConfig: (value as BlockFieldType).config } };
    case ValueType.file:
      return {
        component: (value as FileFieldType).multi ? FileUploadField : SingleFileUploadField,
        props: {
          onUploadFile: (fileEntry: FileEntry) =>
            client.uploadFile(fileEntry.file || new File([], "wont happen")).pipe(
              map((results) => ({
                ...fileEntry,
                ...results,
                status: results.status as unknown as TransitionStatus,
              }))
            ),
        },
      };
  }
  return { component: null, props: {} };
}
