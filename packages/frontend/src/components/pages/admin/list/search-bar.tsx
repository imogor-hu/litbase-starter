import { Search } from "@styled-icons/boxicons-regular";
import { Card, spacing3, spacing4 } from "@litbase/alexandria";
import styled from "styled-components";

export function SearchBar({
  value,
  onChange,
  className,
}: {
  value: string;
  onChange: (value: string) => void;
  className?: string;
}) {
  return (
    <Body className={className}>
      <SearchIcon />
      <Input value={value} onChange={(e) => onChange(e.target.value)} placeholder="Keresés..." />
    </Body>
  );
}

const Input = styled.input`
  outline: none;
  border: none;
  width: 100%;
`;

const SearchIcon = styled(Search)`
  color: ${({ theme }) => theme.gray300};
  font-size: ${({ theme }) => theme.text2xl};
  margin-right: ${spacing4};
`;

const Body = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  grid-column: 1 / -1;
  padding: ${spacing3};

  ${Card} > & {
    padding-left: ${spacing3};
    padding-bottom: ${spacing3};
  }
`;
