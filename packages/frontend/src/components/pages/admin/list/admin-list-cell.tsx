import { FieldType, ValueType } from "../common/types";
import { DocumentInterface } from "@litbase/core";
import { Cell, size24, RichTextDisplay } from "@litbase/alexandria";
import styled from "styled-components";
import { Check, X } from "@styled-icons/boxicons-regular";
import { Block } from "@my-app/common/models/page";

export function AdminListCell({
  value,
  item,
  fieldName,
}: {
  value: FieldType;
  item: DocumentInterface;
  fieldName: string;
}) {
  const castItem = item as unknown as Record<string, unknown>;
  switch (value.type) {
    case ValueType.date:
      return <Cell>{(castItem[fieldName] as Date).toLocaleDateString()}</Cell>;
    case ValueType.number:
      return <Cell>{castItem[fieldName] as number}</Cell>;
    case ValueType.string:
      return <Cell>{castItem[fieldName] as string}</Cell>;
    case ValueType.boolean:
      return <Cell>{(castItem[fieldName] as boolean) ? <Check /> : <X />}</Cell>;
    case ValueType.blocks:
      return <Cell>{(castItem[fieldName] as Block[]).length}db</Cell>;
    case ValueType.richText:
      return (
        <RichTextCell>
          <RichTextDisplay content={castItem[fieldName] as string} />
        </RichTextCell>
      );
  }
  return null;
}

const RichTextCell = styled(Cell)`
  max-height: ${size24};
  overflow-y: auto;
`;
