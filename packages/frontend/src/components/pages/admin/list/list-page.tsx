import { ReactNode, useEffect, useMemo, useState } from "react";
import { useInView } from "react-intersection-observer";
import { debounce } from "lodash-es";
import { Card, Grid, GridBody, HeadCell, spacing4 } from "@litbase/alexandria";
import { DocumentInterface } from "@litbase/core";
import { Collections } from "@my-app/common/models/collections";
import styled from "styled-components";
import { useQuery, useQueryType } from "@litbase/react/hooks/use-query";
import { SearchBar } from "./search-bar";

export function ListPage<T extends DocumentInterface>({
  type,
  searchFields,
  fields,
  rowComponent,
  headCells,
  columns,
  filters,
  buttons,
  enableSearch = true,
}: // filterType,
{
  type: Collections;
  searchFields: string[];
  fields: Record<string, unknown>;
  rowComponent: (item: T, index: number) => ReactNode;
  headCells: ReactNode;
  columns: string;
  filters?: Record<string, unknown>;
  buttons?: ReactNode;
  enableSearch?: boolean;
  // filterType?: FilterType;
}) {
  const { ref, inView } = useInView();
  const [query, setQuery] = useState("");
  const [skip, setSkipOriginal] = useState(0);
  const setSkip = useMemo(() => {
    return debounce(setSkipOriginal, 500, { leading: true, trailing: false });
  }, [setSkipOriginal]);
  const [results, loading] = useQuery<T>(type, {
    $live: true,
    $matchAll: {
      $or: searchFields.map((elem) => ({
        [elem]: { $regex: query + ".*", $options: "i" },
      })),
      ...filters,
    },
    // $skip: skip,
    ...fields,
  });

  useEffect(() => {
    if (inView && !loading) {
      setSkip(skip + 25);
    }
  }, [inView, loading]);

  return (
    <>
      {buttons && <Row>{buttons}</Row>}
      <StyledCard>
        {enableSearch && (
          <SearchBar
            value={query}
            onChange={(query) => {
              setQuery(query);
              setSkip(0);
            }}
          />
        )}
        <Col>
          <Grid columns={columns}>
            {headCells}
            <StyledGridBody emptyLabel="Nincs találat" isEmpty={results?.length === 0} isLoading={loading}>
              {results?.map((elem: T, index: number) => rowComponent(elem, index))}
              <div ref={ref} />
            </StyledGridBody>
          </Grid>
        </Col>
      </StyledCard>
    </>
  );
}

const StyledGridBody = styled(GridBody)``;

const Row = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${spacing4};
`;

const StyledCard = styled(Card)`
  padding: 0;
  max-width: 100%;
  margin-bottom: ${spacing4};
`;

const Col = styled.div`
  min-height: 0;
  overflow: auto;
`;
