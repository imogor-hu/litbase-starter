import { ReactNode } from "react";
import { Helmet } from "react-helmet";

const favicon = "";

export function AppMeta() {
  return (
    <Helmet defaultTitle={"Festo-AM SE"}>
      <html lang={"hu"} />
      <link rel="shortcut icon" href={favicon} />
    </Helmet>
  );
}

export function PageMeta({ title, children }: { title?: string; titleKey?: string; children?: ReactNode }) {
  return (
    <Helmet>
      <title>{title}</title>
      {children}
    </Helmet>
  );
}
