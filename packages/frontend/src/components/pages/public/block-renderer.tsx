import { ComponentType, ReactNode } from "react";
import { Block, BlockType } from "@my-app/common/models/page";
import styled from "styled-components";
import { HeroBlockRenderer } from "./block-renderers/hero-block-renderer";
import { createSlug } from "../../../utils/create-slug";
import { EmptyBlockRenderer } from "./block-renderers/empty-block-renderer";
import { InViewNotifier } from "@litbase/alexandria";

interface BlockRendererProps {
  block: Block;
  onInView: (slug: string) => void;
  children?: ReactNode;
}

export function BlockRenderer({ block, onInView, children }: BlockRendererProps) {
  const slug = createSlug(block.title);
  const Component = blockComponents[block.type];

  return Component ? (
    <>
      <RelativeContainer>
        <InViewNotifier onInView={() => onInView(slug)} deps={[slug]} />
        <Component block={block}>{children}</Component>
      </RelativeContainer>
    </>
  ) : null;
}

const blockComponents = {
  [BlockType.HERO]: HeroBlockRenderer,
  [BlockType.EMPTY]: EmptyBlockRenderer,
} as Record<BlockType, ComponentType<{ block: unknown }>>;

const RelativeContainer = styled.div`
  position: relative;
`;
