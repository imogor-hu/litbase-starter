import { useCallback, useEffect } from "react";
import { Block, BlockType, Page } from "@my-app/common/models/page";
import { createSlug } from "../../../utils/create-slug";
import { BlockRenderer } from "./block-renderer";
import { PageMeta } from "./app-meta";

export function PageRenderer({ page, slug }: { page: Page; slug: string }) {
  useEffect(() => {
    if (slug !== page.slug) {
      const blockSlug = slug.startsWith(page.slug + "/") ? slug.substr(page.slug.length + 1) : slug;
      const matchingIndex = page.blocks.map((elem) => createSlug(elem.title))?.indexOf(blockSlug) ?? -1;

      if (0 <= matchingIndex) {
        window.scrollTo({ top: matchingIndex * window.innerHeight, behavior: "auto" });
      }
    }
  }, [page, slug]);

  const onInView = useCallback(
    (slug) => {
      const pageSlug = page.slug;
      window.history.replaceState(null, "", "/" + pageSlug + (pageSlug ? "/" : "") + slug);
    },
    [page]
  );

  return (
    <>
      <PageMeta title={page.name}>
        {slug !== page.slug && (
          <link rel="canonical" href={location.protocol + "//" + location.host + "/" + page.slug} />
        )}
      </PageMeta>
      {[BlockType.HERO].includes(page?.blocks[0]?.type) ? (
        <BlockRenderer block={page.blocks[0]} onInView={onInView}>
          {page.blocks.slice(1).map((block) => (
            <BlockRenderer key={block.id} block={block} onInView={onInView} />
          ))}
        </BlockRenderer>
      ) : (
        page?.blocks.map((block: Block) => <BlockRenderer key={block.id} block={block} onInView={onInView} />)
      )}
    </>
  );
}
