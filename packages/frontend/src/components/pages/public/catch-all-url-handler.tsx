import { useEffect } from "react";
import { RouteComponentProps } from "react-router-dom";
import { timer } from "rxjs";
import { mapTo } from "rxjs/operators";
import { useObservable } from "@litbase/use-observable";
import { useQuery, useQueryTypeOne } from "@litbase/react/hooks/use-query";
import { Page } from "@my-app/common/models/page";
import { Collections } from "@my-app/common/models/collections";
import { LoadingPage } from "../loading-page";
import { NotFoundPage } from "../not-found-page";
import { PageRenderer } from "./page-renderer";

export function CatchAllUrlHandler({
  match: {
    params: { id = "" },
  },
}: RouteComponentProps<{ id?: string }>) {
  const [showLoading] = useObservable(() => timer(500).pipe(mapTo(true)), false);
  const [content, loading] = useQuery<Page>(Collections.PAGES, {
    $match: { slug: id.replace(/\/.+/g, "") },
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  if (!content) {
    if (loading) {
      if (showLoading) {
        return <LoadingPage />;
      }

      return <div style={{ minHeight: "100vh" }} />;
    } else {
      return <NotFoundPage />;
    }
  }

  return <PageRenderer page={content} slug={id} />;
}
