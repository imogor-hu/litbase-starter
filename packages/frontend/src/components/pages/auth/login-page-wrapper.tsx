import { ReactNode } from "react";
import styled from "styled-components";
import { Card, SubmitButton, useFormikWrapperContext } from "@litbase/alexandria";

export function LoginPageWrapper({ children }: { children: ReactNode }) {
  return (
    <StyledLoginPage>
      <StyledLoginPageContainer>
        <PageTitle>Starter app</PageTitle>
        <LoginFormCard>{children}</LoginFormCard>
      </StyledLoginPageContainer>
    </StyledLoginPage>
  );
}

export function FormikLoginErrorAlert() {
  const { errors } = useFormikWrapperContext<{ submit: void }>((context) => [context.errors]);

  if (!errors["submit"]) return null;

  return <LoginPageAlert>{errors["submit"]}</LoginPageAlert>;
}

const AdminPage = styled.div``;

export const LoginPageAlert = styled(Card).attrs(() => ({ $kind: "error", $padding: [3, 4] }))`
  margin-top: ${({ theme }) => theme.spacing6};
`;

export const LoginSubmitButton = styled(SubmitButton)`
  margin-top: ${({ theme }) => theme.spacing6};
  display: block;
  width: 100%;
`;

const StyledLoginPage = styled(AdminPage)`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  flex: 1;
`;

const StyledLoginPageContainer = styled.div`
  width: 30rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const LoginFormCard = styled(Card)`
  margin-top: ${({ theme }) => theme.spacing8};
`;

export const PageTitle = styled.h1`
  font-size: ${({ theme }) => theme.text3xl};
  font-weight: bold;
  margin: 0;
  line-height: ${({ theme }) => theme.leading9};
`;
