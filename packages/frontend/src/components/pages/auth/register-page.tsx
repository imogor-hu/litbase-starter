import styled from "styled-components";
import { FormikWrapper, FieldWrapper, InputField, CheckboxField, Card } from "@litbase/alexandria";
import { LoginPageWrapper, LoginSubmitButton } from "./login-page-wrapper";
import { Collections } from "@my-app/common/models/collections";
import { AuthRedirecter } from "./auth-redirecter";
import { client } from "@litbase/client";

export function RegisterPage() {
  return (
    <LoginPageWrapper>
      <FormikWrapper
        initialValues={{ name: "", email: "", password: "" }}
        onSubmit={(values) => {
          client.registerWithEmailAndPassword(values).then((e) => {
            return client.litql.query(Collections.USERS, {
              $match: {
                _id: e.user._id,
              },
              $update: {
                $set: {
                  maxReviewsPerDay: 100,
                  newConceptsPerDay: 40,
                },
              },
            });
          });
        }}
      >
        <FieldWrapper name="email" component={InputField} type="email" label="Email" placeholder="admin@email.hu" />
        <FieldWrapper name="password" component={InputField} type="password" label="Password" placeholder="●●●●●●●●" />
        <FieldWrapper name={"name"} label={"Username"} component={InputField} />
        <LoginSubmitButton>Login</LoginSubmitButton>
        <AuthRedirecter mode={"register"} />
      </FormikWrapper>
    </LoginPageWrapper>
  );
}
