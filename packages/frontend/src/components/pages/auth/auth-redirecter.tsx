import { Button, spacing2 } from "@litbase/alexandria";
import styled from "styled-components";
import { Paths } from "../../../utils/paths";

export function AuthRedirecter({ mode }: { mode: "login" | "register" }) {
  return (
    <Body>
      {mode === "login" ? "Don't have an account?" : "Already have an account?"}
      <Button $kind="link" to={mode === "login" ? Paths.REGISTER : Paths.LOGIN}>
        {mode === "login" ? "Sign up" : "Log in"}
      </Button>
    </Body>
  );
}

const Body = styled.div`
  text-align: center;
  margin: ${spacing2};
`;
