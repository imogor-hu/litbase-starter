import { User } from "@my-app/common/models/user";
import { useObservable } from "@litbase/use-observable";
import { isUserLoggedIn } from "../../app";
import { emptyUser, isUserGuest } from "@litbase/core";
import { Observable } from "rxjs";
import { ReactNode } from "react";
import { Switch, Redirect } from "react-router-dom";
import { Paths } from "../../../utils/pages-enum";
import { useCurrentUser } from "@litbase/react";
import styled from "styled-components";
import { client } from "@litbase/client";

export function AuthSwitch({
  authPaths,
  guestPaths,
  adminPaths,
}: {
  authPaths: ReactNode;
  guestPaths: ReactNode;
  adminPaths: ReactNode;
}) {
  const [user] = useObservable<User>(() => client.currentUser$ as Observable<User>, {
    ...emptyUser,
    isAdmin: true,
    name: "notGuest",
    providers: { email: { id: "yes@gmail.com" } },
  });
  const isAuthenticated = !isUserGuest(user) && isUserLoggedIn(user);
  return (
    <>
      <ContentContainer>
        <Switch>
          {isAuthenticated ? (
            <>
              {authPaths}
              <AdminPaths paths={adminPaths} />
              <Redirect to={Paths.MAIN} />
            </>
          ) : (
            guestPaths
          )}
        </Switch>
      </ContentContainer>
    </>
  );
}

function AdminPaths({ paths }: { paths: ReactNode }) {
  const user = useCurrentUser<User>();
  if (!user.isAdmin) {
    return null;
  }
  return <>{paths}</>;
}

const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  height: 100%;
  flex: 1;
`;
