import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Paths } from "../utils/paths";
import { User } from "@my-app/common/models/user";
import { isEmptyUser, isUserGuest } from "@litbase/core";
import { ModalProvider, roundedLg, spacing4, spacing5 } from "@litbase/alexandria";
import { TopScroller } from "@litbase/alexandria/components/utils/top-scroller";
import { ToastContainer } from "react-toastify";
import { useCurrentUser } from "@litbase/react";
import "tippy.js/dist/tippy.css";
import "react-toastify/dist/ReactToastify.css";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import { CaptureConsole } from "@sentry/integrations";
import { LoadingPage } from "./pages/loading-page";
import { theme, GlobalStyle } from "../styles/theme-types";
import { CatchAllUrlHandler } from "./pages/public/catch-all-url-handler";
import { Admin } from "./pages/admin/admin";
import { LoginPage } from "./pages/auth/login-page";
import { NotFoundPage } from "./pages/not-found-page";
import { RegisterPage } from "./pages/auth/register-page";

if (process.env.NODE_ENV !== "dev") {
  Sentry.init({
    dsn: process.env.SENTRY_DNS,
    integrations: [
      new Integrations.BrowserTracing(),
      new CaptureConsole({
        levels: ["error"],
      }),
    ],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
    ignoreErrors: [
      "ResizeObserver loop limit exceeded",
      "ResizeObserver loop completed with undelivered notifications.",
    ],
  });
}

export function App() {
  const user = useCurrentUser<User>();
  if (!user || isEmptyUser(user)) {
    return <LoadingPage />;
  }
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <BrowserRouter>
        <TopScroller />
        <ModalProvider>
          <Row>
            <Col>
              <AuthSwitch />
            </Col>
          </Row>
          <StyledToastContainer autoClose={5000} />
        </ModalProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

function AuthSwitch() {
  const user = useCurrentUser<User>();
  return (
    <>
      <Switch>
        <Route path={Paths.MAIN} exact component={CatchAllUrlHandler} />
        {isUserGuest(user) ? <GuestOnlyPaths /> : <AuthPaths />}
      </Switch>
    </>
  );
}

function RedirectOnNoHit() {
  return <Redirect to={Paths.MAIN} />;
}

function GuestOnlyPaths() {
  return (
    <Switch>
      <Route path={"/login"} component={LoginPage} />
      <Route path={"/register"} component={RegisterPage} />
      <Route path={"/"} component={NotFoundPage} />
    </Switch>
  );
}

function AuthPaths() {
  const user = useCurrentUser<User>();
  return (
    <Switch>
      {user.isAdmin && <Route path={"/admin"} component={Admin} />}
      <RedirectOnNoHit />
    </Switch>
  );
}

const StyledToastContainer = styled(ToastContainer)`
  width: 26rem;

  .Toastify__toast--success,
  .Toastify__toast--error {
    font-family: inherit;
    background-color: white;
    border-radius: ${roundedLg};
    box-shadow: ${({ theme }) => theme.shadowLg};
    color: ${({ theme }) => theme.gray900};
    padding: ${spacing4} ${spacing5};

    .Toastify__close-button {
      color: ${({ theme }) => theme.gray600};
      line-height: 1;
      font-size: ${({ theme }) => theme.textXl};
      margin-top: -0.2rem;

      &:hover {
        color: ${({ theme }) => theme.gray700};
      }

      > svg {
        height: 1em;
        width: auto;
      }
    }
  }

  .Toastify__toast--success .Toastify__progress-bar {
    background-color: ${({ theme }) => theme.green300};
  }

  .Toastify__toast--error .Toastify__progress-bar {
    background-color: ${({ theme }) => theme.red600};
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
`;

const Row = styled.div`
  display: flex;
  max-height: 100vh;
`;
