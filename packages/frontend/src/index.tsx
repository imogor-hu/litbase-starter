import { App } from "./components/app";
import { render } from "react-dom";
import { startLitbaseClient } from "./litbase-client";

startLitbaseClient();

const appElement = document.getElementById("app");

render(<App />, appElement);
