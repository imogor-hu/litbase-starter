import exo2Regular from "../assets/fonts/exo 2/static/Exo2-Regular.ttf";
import exo2Medium from "../assets/fonts/exo 2/static/Exo2-Medium.ttf";
import exo2Bold from "../assets/fonts/exo 2/static/Exo2-Bold.ttf";
import { css } from "styled-components";
import { FontWeight } from "@litbase/alexandria";

export const fonts = css`
  @font-face {
    font-family: "Exo 2";
    font-style: normal;
    font-weight: ${FontWeight.Regular};
    src: url("${exo2Regular}") format("woff2");
  }

  @font-face {
    font-family: "Exo 2";
    font-style: normal;
    font-weight: ${FontWeight.Medium};
    src: url("${exo2Medium}") format("woff2");
  }

  @font-face {
    font-family: "Exo 2";
    font-style: normal;
    font-weight: ${FontWeight.Bold};
    src: url("${exo2Bold}") format("woff2");
  }
`;
