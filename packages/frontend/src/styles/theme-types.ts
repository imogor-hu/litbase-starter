import { baseGlobalStyle, baseTheme, spacing5 } from "@litbase/alexandria";
import { createGlobalStyle, css } from "styled-components";
import { fonts } from "./fonts";

export const theme = {
  ...baseTheme,
  primary100: "hsl(263,48%,95%)",
  primary200: "hsl(261,46%,87%)",
  primary300: "hsl(262,46%,79%)",
  primary400: "hsl(262,46%,63%)",
  primary500: "hsl(262,52%,47%)",
  primary600: "hsl(262,52%,42%)",
  primary700: "hsl(262,51%,28%)",
  primary800: "hsl(261,52%,21%)",
  primary900: "hsl(262,53%,14%)",
};

const commonScrollbarStyle = css`
  scrollbar-width: thin;

  &::-webkit-scrollbar {
    width: 6px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
  }
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;

export const darkScrollbarStyle = css`
  ${commonScrollbarStyle};

  scrollbar-color: white transparent;

  &::-webkit-scrollbar-thumb {
    background: white;
  }
`;

export const lightScrollbarStyle = css`
  ${commonScrollbarStyle};

  scrollbar-color: #888 transparent;

  &::-webkit-scrollbar-thumb {
    background: #888;
  }
`;

export const scrollbarStylesMixin = css<{ $backgroundTone?: "light" | "dark" }>`
  ${({ $backgroundTone = "light" }) => ($backgroundTone === "dark" ? darkScrollbarStyle : lightScrollbarStyle)};
`;

export const GlobalStyle = createGlobalStyle`
  ${baseGlobalStyle};
  ${fonts};

  * {
    ${scrollbarStylesMixin};
  }
  
    body {
      min-height: 100vh;
      font-family: "Exo 2", sans-serif;
    }

    input {
      outline: none !important;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: ${({ theme }) => theme.primary900};
      text-transform: uppercase;
    }

    .tippy-content {
      font-size: ${({ theme }) => theme.textSm};
    }
    
    ul {
      padding-left: ${spacing5};
    }
    
    p, ul {
      color: ${({ theme }) => theme.gray300};
      font-weight: bold;
    }
    
    a {
      text-transform: uppercase;
      font-weight: bold;
    }
`;
