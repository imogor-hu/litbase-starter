import { toast } from "react-toastify";
import { ComponentType, ReactNode } from "react";
import { CheckCircle, Error, InfoCircle } from "@styled-icons/boxicons-solid";
import styled from "styled-components";
import { FontWeight, spacing3 } from "@litbase/alexandria";

export function showErrorToast(content: ReactNode, title?: ReactNode, autoClose = 5000) {
  toast.error(<Toast icon={ToastErrorIcon} title={title} content={content} />, {
    position: "top-right",
    autoClose: autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function showSuccessToast(content: ReactNode, title?: ReactNode, autoClose = 5000) {
  return toast.success(<Toast icon={ToastSuccessIcon} title={title} content={content} />, {
    position: "top-right",
    autoClose: autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function showInfoToast(content: ReactNode, title?: ReactNode, autoClose = 5000) {
  return toast.success(<Toast icon={ToastInfoIcon} title={title} content={content} />, {
    position: "top-right",
    autoClose: autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function InfoToast({ children }: { children: ReactNode }) {
  return <Toast icon={ToastInfoIcon} content={children} />;
}

export function SuccessToast({ children }: { children: ReactNode }) {
  return <Toast icon={ToastSuccessIcon} content={children} />;
}

export function ErrorToast({ children }: { children: ReactNode }) {
  return <Toast icon={ToastErrorIcon} content={children} />;
}

interface ToastProps {
  icon: ComponentType;
  title?: ReactNode;
  content: ReactNode;
}

function Toast({ icon: Icon, title, content }: ToastProps) {
  return (
    <StyledToast>
      <Icon />
      <ToastBody>
        {title && <ToastTitle>{title}</ToastTitle>}
        <ToastContent>{content}</ToastContent>
      </ToastBody>
    </StyledToast>
  );
}

const StyledToast = styled.div`
  display: flex;
  align-items: center;
`;

const ToastIcon = styled.div`
  font-size: ${({ theme }) => theme.text2xl};
  margin-right: ${spacing3};
`;

const ToastSuccessIcon = styled(ToastIcon).attrs(() => ({ as: CheckCircle }))`
  color: ${({ theme }) => theme.green400};
`;

const ToastInfoIcon = styled(ToastIcon).attrs(() => ({ as: InfoCircle }))`
  color: ${({ theme }) => theme.primary300};
`;

const ToastErrorIcon = styled(ToastIcon).attrs(() => ({ as: Error }))`
  color: ${({ theme }) => theme.red600};
`;

const ToastBody = styled.div`
  flex-grow: 1;
  font-size: ${({ theme }) => theme.textSm};
`;

const ToastTitle = styled.div`
  font-weight: ${FontWeight.Medium};
  margin-bottom: ${({ theme }) => theme.spacing1};
`;

const ToastContent = styled.div`
  color: ${({ theme }) => theme.gray600};
`;
