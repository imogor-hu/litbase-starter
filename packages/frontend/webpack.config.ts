import { getConfigFactory } from "../../webpack.config";
import path from "path";

export default getConfigFactory(() => ({
  htmlOptions: {
    template: path.resolve("./src/index.html"),
  },
  outputOptions: {
    filename: "index.[contenthash].js",
    path: path.resolve("./dist"),
    publicPath: "/",
  },
  useSvgLoader: true,
  useSourcemap: true,
}));
